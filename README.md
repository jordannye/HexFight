### How the game will work ###

Up to 4 players, starting with 2.
First work on the Vs AI mode.

Before the game starts you all pick colours, first come first serve from the list.

Whatever you pick is now YOUR COLOUR.
When the game starts your 'Character' will spawn on 1 of 4 corners of the map, all forced to have the colour of your choice on them.

There will be an 'Objective' in the middle.

The aim of the game is the use 'PointyCircles' mechanics, of the joining hex nodes together to create new ones and get points based on the chain size won.

The only difference is that you can only accept chains that end/start with your characters node on them.

Each time you arrive at the 'Objective' you score a point, which will then be multiplied by the score you have accumulated on getting to the 'Objective'