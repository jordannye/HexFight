﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UpdateRoundInfo : MonoBehaviour
{
    public class RoundData
    {
        public string ToShow;
        public CellProperties.ColourType Colour;
        public double Percent;
    }

    private List<Text> _Text = new List<Text>();
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            _Text.Add(GameObject.Find("P" + (i + 1) + "_Info").GetComponent<Text>());
        }

        HexFightGameRound.Instance.ListenPlayerPossesionUpdateCallback((colours, counts) =>
        {
            foreach (Text txt in _Text) { txt.text = ""; }

            List<RoundData> data = new List<RoundData>();

            int total = 0;
            foreach (var point in HexFightGridBehaviour.Instance.HexFightGrid) { total++; }

            for (int i = 0; i < colours.Length; i++)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(colours[i].ToString());
                builder.Append(" : ");
                float percent = ((float)counts[i] / (float)total);
                builder.Append((percent).ToString("P"));
                //builder.Append("%");
                //_Text[i].text = builder.ToString();

                RoundData rData = new RoundData()
                {
                    ToShow = builder.ToString(),
                    Colour = colours[i],
                    Percent = percent
                };
                data.Add(rData);
            }

            List<RoundData> orderedData = data.OrderBy(d => d.Percent).ToList();
            orderedData.Reverse();
            for (int j = 0; j < orderedData.Count; j++)
            {
                _Text[j].text = orderedData[j].ToShow;
            }
        });
    }
}
