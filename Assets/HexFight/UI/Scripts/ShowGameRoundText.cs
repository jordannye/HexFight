﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowGameRoundText : MonoBehaviour
{
    private Text GameRoundText;

    // Use this for initialization
    void Start()
    {
        GameRoundText = GetComponent<Text>();
        HexFightGameRound.Instance.ListenStartNewPlayerTurn((newRound, playCol) =>
        {
            string round = newRound.ToString();
            GameRoundText.text = "< " + round + " >";
        });

        HexFightGameRound.Instance.ListenEndGameCallback(newRound =>
        {
            string round = newRound.ToString();
            GameRoundText.text = "< " + round + " >";
        });
    }
}
