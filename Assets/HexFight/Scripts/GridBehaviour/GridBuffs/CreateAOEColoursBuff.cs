﻿using Gamelogic.Grids;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

public class CreateAOEColoursBuff : GridBuff
{
    public override void ApplyBuff(PointyHexPoint point, CellProperties.ColourType playerType)
    {
        GameObject playerObj = PlayerController.Instance.GetPlayerObject(playerType);
        if (playerObj == null) return;
        HexPlayer player = playerObj.GetComponent<HexPlayer>();
        if (player == null) return;

        IGrid<HexFightCell, PointyHexPoint> grid = HexFightGridBehaviour.Instance.HexFightGrid;

        HexFightCell cell = grid[point];
        IEnumerable<PointyHexPoint> neighbours = grid.GetNeighbors(point);
        List<PointyHexPoint> changedNeighbours = new List<PointyHexPoint>();

        foreach (PointyHexPoint neighbour in neighbours)
        {
            HexFightCell neighbourCell = grid[neighbour];
            if (!neighbourCell.Active) continue;
            neighbourCell.CurrentColourType = playerType;
            changedNeighbours.Add(neighbour);
        }

        PhotonNetwork.Destroy(gameObject);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }
}
