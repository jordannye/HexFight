﻿using Gamelogic.Grids;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GridBuff : MonoBehaviour
{
    public abstract void ApplyBuff(PointyHexPoint point, CellProperties.ColourType playerType);
}
