﻿using Gamelogic.Grids;
using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HexFightGridBehaviour : GridBehaviour<PointyHexPoint>
{
    public static HexFightGridBehaviour Instance;

    [SerializeField]
    private bool _VerifyingClick = false;

    private bool _AllowClicking = false;

    public IGrid<HexFightCell, PointyHexPoint> HexFightGrid { get; private set; }

    private List<Action> GridCreated = new List<Action>();
    public void AddGridCreationListner(Action invoked) { GridCreated.Add(invoked); }

    private bool _GridCreated = false;
    private bool _StartCalled = false;
    private bool _OnlyOnce = false;

    public class WinChain
    {
        /// <summary>The type that was originally clicked</summary>
        public CellProperties.ColourType ClickedType;
        /// <summary>The origin click point of this chain</summary>
        public PointyHexPoint OriginPoint;
        /// <summary>The list of points that this chain consists of</summary>
        public List<PointyHexPoint> PointChain = new List<PointyHexPoint>();
        /// <summary>How much each hex in the chain is worth</summary>
        public List<double> WinPerHex = new List<double>();
        /// <summary>List of types for each hexcell in the chain to switch to</summary>
        public List<CellProperties.ColourType> SwitchToType = new List<CellProperties.ColourType>();

        /// <summary>The player type that owns this win chain</summary>
        public CellProperties.ColourType OwnerType { get; set; }
    }

    public void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    public void Start()
    {
        _StartCalled = true;
        _AllowClicking = false;
    }

    [PunRPC]
    public void SetClickingActive(bool active)
    {
        //If you are wanting to set the grid active again and are still in the pre game round state, dont
        if (active && HexFightGameRound.Instance.CurrentRoundState == HexFightGameRound.GameState.PRE_GAME) return;
        _AllowClicking = active;
    }

    [PunRPC]
    public void SetVerifyingClick(bool value)
    {
        _VerifyingClick = value;
    }

    public override void InitGrid()
    {
        HexFightGrid = Grid.CastValues<HexFightCell, PointyHexPoint>();

        foreach (PointyHexPoint hexPoint in HexFightGrid)
        {
            HexFightGrid[hexPoint].CurrentColourType = CellProperties.ColourType.TOTAL_COLOURS;
            HexFightGrid[hexPoint].Active = true;
        }

        _GridCreated = true;
    }

    public void Update()
    {
        if (_GridCreated && _StartCalled && !_OnlyOnce)
        {
            _OnlyOnce = true;
            CreateNewGameLayout(0);
        }
    }

    public void CreateNewGameLayout(int seed)
    {
        foreach (PointyHexPoint point in HexFightGrid)
        {
            HexFightGrid[point].CurrentColourType = ((CellProperties.ColourType)UnityEngine.Random.Range(0, (int)CellProperties.ColourType.TOTAL_COLOURS));
        }

        foreach (Action gridCreated in GridCreated) gridCreated.Invoke();

        if (PhotonNetwork.IsMasterClient) return;
        PhotonView pv = PhotonView.Get(this);
        pv.RPC("RequestUpdatedGridState", RpcTarget.MasterClient);
    }

    /// <summary>
    /// Using the nodes nearby construct a distrubution table to favour those of neighbouring colours
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public CellProperties.ColourType GenerateNewColourType(PointyHexPoint point)
    {
        List<PointyHexPoint> neighbours = HexFightGrid.GetNeighbors(point).ToList();

        //Default all weights to 1
        int[] weightings = new int[(int)CellProperties.ColourType.TOTAL_COLOURS];
        for (int i = 0; i < weightings.Length; i++) weightings[i]++;

        weightings[((int)HexFightGrid[point].CurrentColourType)] = 0;

        //And set all the type values to their incrementing int values
        int[] choices = new int[(int)CellProperties.ColourType.TOTAL_COLOURS];
        for (int i = 0; i < choices.Length; i++)
        {
            choices[i] = i;
        }

        //For each neighbour, ask for its type and convert that to its int format.
        //Inverse relationship as the int value goes up the weighting value is decreased.
        int max = (int)CellProperties.ColourType.TOTAL_COLOURS;
        for (int i = 0; i < neighbours.Count; i++)
        {
            if (!HexFightGrid[neighbours[i]].Active) continue;
            int value = ((int)HexFightGrid[neighbours[i]].CurrentColourType);
            value = (max - value) * 2;//multiply the value to increase the gap between lower/higher weightings
            weightings[((int)HexFightGrid[neighbours[i]].CurrentColourType)] += value;
        }

        return ((CellProperties.ColourType)WeightedRandom.ReturnRandomFromWeightings(weightings, choices));
    }

    private void ToggleNeighbours(PointyHexPoint point)
    {
        HexFightCell node = HexFightGrid[point];
        IEnumerable<PointyHexPoint> neighbours = HexFightGrid.GetNeighbors(point);
        foreach (PointyHexPoint neighbour in neighbours)
        {
            if (HexFightGrid[neighbour].CurrentColourType != node.CurrentColourType) continue;
            HexFightGrid[neighbour].CurrentColourType = GenerateNewColourType(neighbour);
        }
    }

    /// <summary>
    /// Attemps to grab all of the connecting nodes of the same type around the currently selected node.
    /// </summary>
    /// <param name="pickedColor"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    private List<PointyHexPoint> GetAllConnectedNodes(CellProperties.ColourType pickedColor, PointyHexPoint point)
    {
        //Get all the originally connecting nodes, including the first one.
        List<PointyHexPoint> neighbourNodes = GetNeighboursSameType(pickedColor, point);

        if (neighbourNodes == null || neighbourNodes.Count == 0) return null;

        //currentWin will now only hold nodes that lead to a dead end and the start
        //A dead end is a node with no more neighbours of the same type
        List<PointyHexPoint> currentWin = new List<PointyHexPoint>();
        currentWin.Add(neighbourNodes[0]);

        neighbourNodes.RemoveAt(0);

        //While you still have neighbours to check, keep adding them to the win
        while (neighbourNodes.Count > 0)
        {
            //Grab the first neighbour to check for more neighbours, remove it from the neighbour list and add it to the win list.
            PointyHexPoint currentNode = neighbourNodes[0];
            currentWin.Add(neighbourNodes[0]);
            neighbourNodes.RemoveAt(0);

            //Ask for all its neighbours, if the array it returns back has more than 1 thing in it then remove the first one (as that is itself) and then add them to the neighbournodes
            List<PointyHexPoint> temp = GetNeighboursSameType(pickedColor, currentNode);
            //Before adding the neighbours into the neighbour array, make sure they are not already part of the currentWin or neighbourNodes
            for (int i = 0; i < temp.Count; i++)
            {
                PointyHexPoint testNode = temp[i];

                //If the neighbouring node isn't already part of the currentWin or the neighbouring nodes, then add it to the neighbouringNodes
                if (!currentWin.Contains(testNode) && !neighbourNodes.Contains(testNode))
                {
                    neighbourNodes.Add(temp[i]);
                }
            }
        }

        return currentWin.ToList();
    }

    /// <summary>
    /// Get all of the neighbours for a certain node, make sure the neighbours are active AND of the same type as the original colour clicked
    /// </summary>
    /// <param name="type"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    private List<PointyHexPoint> GetNeighboursSameType(CellProperties.ColourType type, PointyHexPoint point)
    {
        List<PointyHexPoint> list = HexFightGrid.GetNeighbors(point).ToList();
        List<PointyHexPoint> newList = new List<PointyHexPoint>();
        if (HexFightGrid[point].Active) newList.Add(point);
        for (int i = 0; i < list.Count; i++)
        {
            if (HexFightGrid[list[i]].CurrentColourType == type && HexFightGrid[list[i]].Active) newList.Add(list[i]);
        }
        return newList;
    }

    public void OnClick(PointyHexPoint point)
    {
        if (_VerifyingClick) return;

        //You have clicked your grid.
        //Now you need to tell the master to see if he can create a WinChain for you
        PhotonView photonView = PhotonView.Get(this);
        PlayerBehaviour myPlayer = GameObject.Find("MyPlayer").GetComponent<PlayerBehaviour>();
        photonView.RPC("CheckClick", RpcTarget.MasterClient, point.X, point.Y, (int)myPlayer.PlayerType);
        _VerifyingClick = true;
    }

    /// <summary>
    /// Function for the master only, will check to see if the click made by a certain type of player is valid on the game grid.
    /// If it is then it will start an animation, calling the start animation function on the player type that asks for it to be valid
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="colType"></param>
    [PunRPC]
    public void CheckClick(int x, int y, int colType)
    {//Master only

        if (!PhotonNetwork.IsMasterClient) return;

        CellProperties.ColourType colourType = (CellProperties.ColourType)colType;

        //You are the master, check if a win chain exists for this player on these coordinates

        PointyHexPoint point = new PointyHexPoint(x, y);

        if (!_AllowClicking || !HexFightGrid[point].Active || !HexFightGameRound.Instance.IsMyTurn((CellProperties.ColourType)colType))
        {
            GetComponent<PhotonView>().RPC("SetVerifyingClick", RpcTarget.AllBuffered, false);
            return;
        }

        List<PointyHexPoint> nodes = GetAllConnectedNodes(HexFightGrid[point].CurrentColourType, point);

        //Generate the new WinChain object and populate it with all the data you will need to perform/react to the win
        WinChain chain = new WinChain();
        chain.OwnerType = colourType;
        chain.OriginPoint = point;
        chain.PointChain = nodes;
        chain.SwitchToType = new List<CellProperties.ColourType>();
        chain.ClickedType = HexFightGrid[point].CurrentColourType;

        //Check if the win chain contains the player that asks for it
        GameObject myPlayer = PlayerController.Instance.GetPlayerObject(colourType);
        HexPlayer player = myPlayer.GetComponent<HexPlayer>();

        bool movePlayer = false;

        //Loop through all of the photon player colours, and use the photon id's of the objects to find each player
        foreach (CellProperties.ColourType playerKey in PlayerController.Instance.ActivePhotonIDs.Keys)
        {
            GameObject playerObj = PlayerController.Instance.GetPlayerObject(playerKey);
            HexPlayer playScipt = playerObj.GetComponent<HexPlayer>();

            //Does this win chain contain this player?
            if (chain.PointChain.Contains(playScipt.GridPoint))
            {
                //If the win chain contains a player that isn't the player asking for a chain. Then you can't use it
                //If you are trying to click on a player, then you can't create a win chain either
                if (playScipt.PlayerType != colourType || playScipt.GridPoint == point)
                {
                    GetComponent<PhotonView>().RPC("SetVerifyingClick", RpcTarget.AllBuffered, false);
                    return;
                }

                //You want to tell the hex animations to move this player when each hex is animated
                movePlayer = true;
            }
        }

        //Deactivate all of the nodes that are in this win chain
        foreach (PointyHexPoint node in chain.PointChain)
        {
            HexFightGrid[node].Active = false;
        }
        //You need to deactivate all nodes in the chain first, so you can generate the new type without the influence of the types around it

        //Generate new colour types for the new win chain nodes
        foreach (PointyHexPoint node in chain.PointChain)
        {
            chain.SwitchToType.Add(GenerateNewColourType(node));
        }

        //If the winChain is connected to the player's node, then reverse the order of the winChain and move the player along it
        if (movePlayer)
        {
            //TODO Create the best path towards the end goal instead of just using them all.
            chain.PointChain.Reverse();

            //The last node you land on needs to change to be the same colour type as the player
            chain.SwitchToType[chain.SwitchToType.Count - 1] = chain.ClickedType;
        }

        //Convert win chain into data
        //TODO Make win chain serializable

        int[][] winPositions = new int[chain.PointChain.Count][];
        for (int i = 0; i < winPositions.Length; i++)
        {
            winPositions[i] = new int[] { chain.PointChain[i].X, chain.PointChain[i].Y };
        }
        int[] switchToType = new int[chain.PointChain.Count];
        for (int i = 0; i < winPositions.Length; i++)
        {
            switchToType[i] = (int)chain.SwitchToType[i];
        }

        PhotonView photonView = PhotonView.Get(HexAnimationController.Instance);
        photonView.RPC("StartAnimation", RpcTarget.AllViaServer,
            (int)chain.OwnerType,//The player type that owns this winchain
            new int[] { (int)chain.OriginPoint.X, (int)chain.OriginPoint.Y },//The origin of this winchain
            winPositions,//the x,y positions of the win chain
            switchToType,//The ColourType for this hex to switch to during the animation
            (int)chain.ClickedType,//The type of the hex that was originally clicked
            movePlayer//Whether your not to move the player during the win chain
            );
    }

    [PunRPC]
    public void RequestUpdatedGridState()
    {
        List<int[]> coords = new List<int[]>();
        List<int> colours = new List<int>();
        List<bool> actives = new List<bool>();

        foreach (PointyHexPoint cell in HexFightGrid)
        {
            coords.Add(new int[] { cell.X, cell.Y });
            colours.Add((int)HexFightGrid[cell].CurrentColourType);
            actives.Add(HexFightGrid[cell].Active);
        }

        int[][] coordsArray = coords.ToArray();
        int[] coloursArray = colours.ToArray();
        bool[] activesArray = actives.ToArray();

        PhotonView pv = PhotonView.Get(this);
        pv.RPC("UpdateGridState", RpcTarget.OthersBuffered, coordsArray, coloursArray, activesArray);
    }

    [PunRPC]
    public void UpdateGridState(int[][] coords, int[] colours, bool[] actives)
    {
        for (int i = 0; i < coords.Length; i++)
        {
            int[] ary = coords[i];
            PointyHexPoint point = new PointyHexPoint(ary[0], ary[1]);
            HexFightGrid[point].CurrentColourType = (CellProperties.ColourType)colours[i];
            HexFightGrid[point].Active = (bool)actives[i];
        }
    }
}
