﻿using Gamelogic.Grids;
using Gamelogic.Grids.Examples;
using System.Linq;
using UnityEngine;

public static class CellProperties
{
    public enum ColourType
    {
        L_BLUE,
        L_PURPLE,
        L_YELL,
        L_PINK,
        D_BLUE,
        D_GREEN,
        ORANGE,
        D_RED,
        TOTAL_COLOURS
    }

    public static Color[] Colours = new Color[]
    {
        ExampleUtils.Colors[0],//LIGHT BLUE
        new Color(192f/255f,71f/255f,239f/255f),//LIGHT PURPLE
        ExampleUtils.Colors[2],//LIGHT YELLOW
        ExampleUtils.Colors[3],//LIGHT PINK
        new Color(30f/255f,122f/255f,137f/255f),//DARK BLUE
        new Color(32f/255f,109f/255f,12f/255f),//DARK GREEN
        new Color(239f/255f,164f/255f,2f/255f),//ORANGE
        ExampleUtils.Colors[7],//DARK RED
        ExampleUtils.Colors.Last()
    };
}

public class HexFightCell : SpriteCell
{
    private bool _Active = true;
    public bool Active
    {
        get
        {
            return _Active;
        }
        set
        {
            _Active = value;
            _Highlight.SetActive(!value);
        }
    }

    private CellProperties.ColourType _ColourType;

    public CellProperties.ColourType CurrentColourType
    {
        get { return _ColourType; }
        set
        {
            _ColourType = value;

            if ((int)value >= CellProperties.Colours.Length) return;
            //TODO Fix why you get large number through when client connecting to game in progress
            Color = CellProperties.Colours[(int)_ColourType];
        }
    }

    //**** OBJECTS ON THIS CELL *** //

    /// <summary>One player per cell</summary>
    public HexPlayer PlayerOnCell { get; set; }

    private GameObject _GridBuff;
    public GameObject GridBuff
    {
        get { return _GridBuff; }
        set
        {
            if (_GridBuff != null) Destroy(_GridBuff);
            _GridBuff = value;
            _GridBuff.transform.position = transform.position;
        }
    }

    private GameObject _Highlight;

    public new void Awake()
    {
        base.Awake();

        _Highlight = transform.Find("Sprite").transform.Find("Highlight").gameObject;
        _Highlight.SetActive(false);

    }

    public void OnClick()
    {

    }

}
