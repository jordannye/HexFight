﻿using Photon.Pun;
using UnityEngine;

public class ShowConnectionStatus : Photon.Pun.MonoBehaviourPun
{
    private void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.NetworkClientState.ToString());
    }
}
