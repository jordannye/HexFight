﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnPopup : MonoBehaviour
{
    private SpriteRenderer _HexagonSprite;

    /// <summary>Speed at which the animation takes ?? Yah i dunno</summary>
    public float AnimSpeed = 1f;

    /// <summary>True if the animation is occuring</summary>
    private bool _Animating = false;

    /// <summary>The time to taken to get from Start->End</summary>
    private float _StartTime;

    private bool _ScalingUp;
    private bool _ScalingDown;

    public float _WaitingSeconds = 1f;

    // Use this for initialization
    void Start()
    {
        if (_HexagonSprite != null) return;
        _HexagonSprite = transform.Find("Hexagon").GetComponent<SpriteRenderer>();
    }

    public void Animate(CellProperties.ColourType colType)
    {
        _HexagonSprite = transform.Find("Hexagon").GetComponent<SpriteRenderer>();
        _HexagonSprite.color = CellProperties.Colours[(int)colType];
        _ScalingUp = true;
        _StartTime = Time.time;
        _Animating = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_Animating) return;
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            _Animating = false;
            Destroy(gameObject);
        }

        //Scale up to max size
        if (_ScalingUp)
        {
            float dist = (Time.time - _StartTime) * AnimSpeed;
            float frac = dist / 1;

            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, frac);

            if (frac < 1) return;
            _ScalingUp = false;
        }
        else if (_ScalingDown)
        {
            float dist = (Time.time - _StartTime) * AnimSpeed;
            float frac = dist / 1;

            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, frac);

            if (frac < 1) return;
            _Animating = false;
            Destroy(gameObject);
        }
        else
        {
            _WaitingSeconds -= Time.deltaTime;
            if (_WaitingSeconds <= 0f)
            {
                _ScalingDown = true;
                _StartTime = Time.time;
            }
        }
    }
}
