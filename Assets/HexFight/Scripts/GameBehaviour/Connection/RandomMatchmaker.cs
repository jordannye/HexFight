﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RandomMatchmaker : Photon.Pun.MonoBehaviourPunCallbacks
{
    public static RandomMatchmaker Instance;

    private string _RoomName = "4PRoom_";
    private int _RoomID = 0;

    // Use this for initialization
    void Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
        TryConnect();
    }

    private void TryConnect()
    {
        PlayerPrefs.DeleteKey("PUNCloudBestRegion");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    private void MakeAndJoinRoom()
    {
        PhotonNetwork.JoinOrCreateRoom("4PRoom_" + _RoomID++, new RoomOptions()
        {
            MaxPlayers = 4,
            IsVisible = true,
            IsOpen = true,
            PublishUserId = true
        }, TypedLobby.Default);
    }

    #region Overrides of MonoBehaviourPunCallbacks

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        MakeAndJoinRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        MakeAndJoinRoom();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        MakeAndJoinRoom();
    }

    #endregion

    public override void OnJoinedRoom()
    {
        Debug.Log("Connected to room " + PhotonNetwork.CurrentRoom);
        PhotonNetwork.IsMessageQueueRunning = false;
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneManager_activeSceneChanged;
        PhotonNetwork.LoadLevel("4P_GameScene");
    }

    private void SceneManager_activeSceneChanged(Scene arg0, LoadSceneMode arg1)
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= SceneManager_activeSceneChanged;
        PhotonNetwork.Instantiate("Prefabs/Players/MyPlayer", Vector3.zero, Quaternion.identity).name = "MyPlayer";
    }

    public void ReMatch()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("4P_Rematch");
    }
}
