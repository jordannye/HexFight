﻿using Photon.Pun;
using System.Collections.Generic;

public class DisconnectionController : Photon.Pun.MonoBehaviourPunCallbacks
{
    public static DisconnectionController Instance;

    class PlayerInfo
    {
        public int PhotonID;
        public CellProperties.ColourType PlayerType;
        public Photon.Realtime.Player PhotonPlayer;
    }

    Dictionary<Photon.Realtime.Player, PlayerInfo> PlayerGameObjects = new Dictionary<Photon.Realtime.Player, PlayerInfo>();

    // Use this for initialization
    void Start()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Overrides of MonoBehaviourPunCallbacks

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        PlayerInfo pInfo = PlayerGameObjects[otherPlayer];
        PlayerGameObjects.Remove(otherPlayer);
        PlayerController.Instance.RemovePlayer(pInfo.PlayerType);
        HexFightGameRound.Instance.photonView.RPC("PlayersConnectedUpdate", RpcTarget.AllBuffered, PhotonNetwork.PlayerList.Length);
    }

    #endregion

    [PunRPC]
    public void RegisterPlayerObject(string playerID, int colourType, int photonID)
    {
        Photon.Realtime.Player PPlayer = null;
        foreach (Photon.Realtime.Player pp in PhotonNetwork.PlayerList) if (pp.UserId == playerID) PPlayer = pp;

        if (PPlayer == null || PlayerGameObjects.ContainsKey(PPlayer)) return;

        PlayerGameObjects.Add(PPlayer, new PlayerInfo()
        {
            PhotonID = photonID,
            PlayerType = (CellProperties.ColourType)colourType,
            PhotonPlayer = PPlayer
        });
    }
}
