﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

/// <summary>
/// This class will be what each player controls its player object with
/// This class will ask the grid for information, and then using that information ask the PlayerControll to move your player object
/// to where you want it to go
/// </summary>
public class PlayerBehaviour : Photon.Pun.MonoBehaviourPun
{
    public CellProperties.ColourType PlayerType = CellProperties.ColourType.TOTAL_COLOURS;

    // Use this for initialization
    void Start()
    {
        HexFightGridBehaviour.Instance.AddGridCreationListner(() =>
        {
            StartCoroutine(SendPlayerRequest());
        });
    }

    private IEnumerator SendPlayerRequest()
    {
        yield return new WaitForSeconds(0.2f);
        //When the grid has been created, then you need to ask to create your player
        PhotonView photonView = PlayerController.Instance.GetComponent<PhotonView>();
        photonView.RPC("RequestPlayer", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.UserId);
    }

    [PunRPC]
    public void ReceivePlayer(string photonPlayerID, CellProperties.ColourType playerType, Vector3 spawnPos, int[] gridPos)
    {
        //The master player controller will send back the starting x/y and the colour of your player

        PlayerType = playerType;

        transform.Find("Border_Bot").GetComponent<SpriteRenderer>().color = CellProperties.Colours[(int)playerType];

        GameObject playerObj = PlayerController.Instance.CreatePlayer(playerType, spawnPos, gridPos);

        PlayerController.Instance.photonView.RPC("RegisterPlayerInfo", RpcTarget.AllBuffered,
            (int)playerType, playerObj.GetPhotonView().ViewID, photonPlayerID, gridPos);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void NodeClicked(HexFightGridBehaviour.WinChain winChain)
    {
        int photonViewID = PlayerController.Instance.ActivePhotonIDs[PlayerType];
        if (winChain.OriginPoint == PhotonView.Find(photonViewID).gameObject.GetComponent<HexPlayer>().GridPoint) return;

        GameObject movePlayer = PlayerController.Instance.GetPlayerObject(PlayerType);
        HexAnimationController.Instance.StartAnimation(winChain, movePlayer);
    }
}
