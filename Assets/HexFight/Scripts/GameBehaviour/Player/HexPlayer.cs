﻿using Gamelogic.Grids;
using Photon.Pun;
using UnityEngine;

/// <summary>
/// This class is part of the player object, and is what you place on top of the grid to show where your player is
/// </summary>
public class HexPlayer : Photon.Pun.MonoBehaviourPun
{
    /// <summary>When it is the players turn, this will be active to show whos turn it is</summary>
    private bool _Animating = false;
    /// <summary>When it is the players turn, this will be active to show whos turn it is. Setting this inactive will disable the highlight</summary>
    public bool Animating
    {
        get { return _Animating; }
        set
        {
            _Animating = value;
            if (!value) _PlayerHighlight.SetActive(false);
        }
    }
    private float CurrentAnimateTime = 0f;
    private float FlashAnimTime = 1.2f;

    private SpriteRenderer _PlayerSprite;
    private GameObject _PlayerHighlight;

    private PointyHexPoint _GridPoint;
    public PointyHexPoint GridPoint
    {
        get
        {
            return _GridPoint;
        }
        set
        {
            _GridPoint = value;
            GridX = _GridPoint.X;
            GridY = _GridPoint.Y;
        }
    }
    [SerializeField]
    private int GridX;
    [SerializeField]
    private int GridY;

    public CellProperties.ColourType PlayerType = CellProperties.ColourType.TOTAL_COLOURS;

    public int Score;

    public void Awake()
    {
        _PlayerSprite = transform.Find("Player_Sprite").GetComponent<SpriteRenderer>();
        _PlayerHighlight = _PlayerSprite.transform.Find("Player_Highlight").gameObject;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!Animating) return;
        CurrentAnimateTime += Time.deltaTime;
        if (CurrentAnimateTime < FlashAnimTime) return;
        CurrentAnimateTime = 0;
        _PlayerHighlight.SetActive(!_PlayerHighlight.activeInHierarchy);
    }

    public void SnapToPosition(PointyHexPoint point)
    {
        gameObject.transform.position = HexFightGridBehaviour.Instance.HexFightGrid[point].transform.position;
        GridPoint = point;
    }
}
