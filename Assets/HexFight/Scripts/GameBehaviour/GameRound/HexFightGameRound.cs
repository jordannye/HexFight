﻿using Gamelogic.Grids;
using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HexFightGameRound : Photon.Pun.MonoBehaviourPun
{
    public static HexFightGameRound Instance;

    /// <summary>The type of game round this mode is</summary>
    public enum GameRoundType
    {
        TURN_BASED
    }

    /// <summary>How this game round will function</summary>
    public GameRoundType RoundType;

    public enum GameState
    {
        OTHER,//If anything happens, or goes wrong.
        PRE_GAME,//Waiting for all players to join
        IN_GAME,//Playing the game
        END_GAME,//End of the game screen
    }

    /// <summary>The current state the game round is in, it will start in OTHER but if it isn't changed correctly something has gone wrong</summary>
    public GameState CurrentRoundState = GameState.OTHER;
    /// <summary>Updates the round type to what you pass in</summary>
    /// <param name="newType"></param>
    [PunRPC]
    public void SetRoundState(GameState newType) { CurrentRoundState = newType; }

    //************** GAME ROUND INFO ****************//

    /// <summary>How many game rounds have to go by before the game is over</summary>
    public int MaxGameRounds = 10;

    /// <summary>The current game round turn, increments when all players have taken their turns</summary>
    public int CurrentGameRoundTurn;
    /// <summary>The index of the current player to access PlayerController.ActivePlayerIDs with</summary>
    public int ActivePlayerIndexTurn;

    /// <summary>How many players have connected</summary>
    public int PlayersConnected = 0;

    /// <summary>The current colours turn</summary>
    public CellProperties.ColourType CurrentColoursTurn;
    /// <summary>Set the current colours turn</summary>
    /// <param name="newColoursTurn"></param>
    public void SetColoursTurn(CellProperties.ColourType newColoursTurn) { CurrentColoursTurn = newColoursTurn; }

    /// <summary>List of callbacks for when a new round will start</summary>
    private List<Action<int, CellProperties.ColourType>> StartNewPlayerTurn = new List<Action<int, CellProperties.ColourType>>();
    /// <summary>Add a callback action which will be called upon a new round being started, the new round being started will be passed in </summary>
    /// <param name="newRoundStartCallback"></param>
    public void ListenStartNewPlayerTurn(Action<int, CellProperties.ColourType> newRoundStartCallback) { StartNewPlayerTurn.Add(newRoundStartCallback); }
    /// <summary>Triggers all of the callbacks that are attached for a new round starting</summary>
    [PunRPC]
    public void TriggerStartNewPlayerTurn() { foreach (Action<int, CellProperties.ColourType> act in StartNewPlayerTurn) { act.Invoke(CurrentGameRoundTurn, CurrentColoursTurn); } }

    /// <summary>List of callbacks for when the game ends and you should show the last screen</summary>
    private List<Action<int>> _EndGameCallbacks = new List<Action<int>>();
    /// <summary>Add a callback action which will be called upon a the game ending screen being shown</summary>
    /// <param name="newEndGameCallback"></param>
    public void ListenEndGameCallback(Action<int> newEndGameCallback) { _EndGameCallbacks.Add(newEndGameCallback); }
    /// <summary>Triggers all of the callbacks that are attached for a game ending</summary>
    [PunRPC]
    public void TriggerEndGameCallbacks() { foreach (Action<int> act in _EndGameCallbacks) { act.Invoke(MaxGameRounds); } }


    /// <summary>Action to be called with the information required to find out the amount of squares owned by each player on the board
    /// List of active player colours, amounts of hexagons owned by each colour</summary>
    private List<Action<CellProperties.ColourType[], int[]>> _PlayerPossesionUpdate = new List<Action<CellProperties.ColourType[], int[]>>();
    /// <summary>Add a callback action which will be called upon any change to the possession of grid squares</summary>
    /// <param name="newEndGameCallback"></param>
    public void ListenPlayerPossesionUpdateCallback(Action<CellProperties.ColourType[], int[]> newPlayerPossessionUpdate) { _PlayerPossesionUpdate.Add(newPlayerPossessionUpdate); }
    /// <summary>Triggers all of the callbacks that are attached to listen for any changes in the possession of board tiles</summary>
    [PunRPC]
    public void TriggerPlayerPossesionUpdateCallback(int[] activePlayerColours, int[] playerColoursOwnedHexagonCount)
    {
        CellProperties.ColourType[] colourTypes = activePlayerColours.Cast<CellProperties.ColourType>().ToArray();
        foreach (Action<CellProperties.ColourType[], int[]> act in _PlayerPossesionUpdate)
        {
            act.Invoke(colourTypes, playerColoursOwnedHexagonCount);
        }
    }

    /// <summary>List of callbacks which are called upon a player being moved and landing on another grid square.
    /// Sending the point of the cell landed, and the colour of the player which landed there</summary>
    private List<Action<int[], CellProperties.ColourType>> _PlayerLandedOnNodeCallbacks = new List<Action<int[], CellProperties.ColourType>>();

    /// <summary>Listen to when a player is moved onto another grid node</summary>
    /// <param name="listener"></param>
    public void ListenPlayerLandNodeCallback(Action<int[], CellProperties.ColourType> listener) { _PlayerLandedOnNodeCallbacks.Add(listener); }

    public void PlayerLandedOnNode(int[] point, int playerType)
    {
        foreach (Action<int[], CellProperties.ColourType> act in _PlayerLandedOnNodeCallbacks)
        {
            act.Invoke(point, (CellProperties.ColourType)playerType);
        }

        if (!PhotonNetwork.IsMasterClient) return;
    }

    // **************** --------------------- **************** //

    /// <summary>
    /// Returns true if it is the turn for the type you pass in
    /// </summary>
    /// <param name="colType"></param>
    /// <returns></returns>
    public bool IsMyTurn(CellProperties.ColourType colType)
    {
        return PlayerController.Instance.ActivePlayerIDs.ElementAt(ActivePlayerIndexTurn).Key == colType;
    }

    /// <summary>
    /// Called after the end of a players animation has finished
    /// </summary>
    /// <param name="colType"></param>
    [PunRPC]
    public void FinishPlayerTurn(int colType)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        PhotonView gridBehav = HexFightGridBehaviour.Instance.GetComponent<PhotonView>();
        gridBehav.RPC("SetVerifyingClick", RpcTarget.AllBuffered, false);

        SendPlayerOwnedPercentages();

        //Check to see the current players turn was actually the one that just ended
        //This could be different on disconnects
        if (PlayerController.Instance.ActivePlayerIDs.ElementAt(ActivePlayerIndexTurn).Key == (CellProperties.ColourType)colType)
        {
            ActivePlayerIndexTurn++;
        }

        //If you have reached the end of the player count then the turn is over.
        //Reset the index and increment the round
        if (ActivePlayerIndexTurn >= PlayerController.Instance.ActivePlayerIDs.Count)
        {
            ActivePlayerIndexTurn = 0;
            CurrentColoursTurn = PlayerController.Instance.ActivePlayerIDs.ElementAt(ActivePlayerIndexTurn).Key;
            CurrentGameRoundTurn++;

            //If you haven't reached the turn limit
            if (CurrentGameRoundTurn <= MaxGameRounds)
            {
                photonView.RPC("StartTurn", RpcTarget.AllBuffered, ActivePlayerIndexTurn, CurrentGameRoundTurn, CurrentColoursTurn);
            }
            else
            {
                photonView.RPC("ShowEndScreen", RpcTarget.All);
            }
        }
        else
        {
            CurrentColoursTurn = PlayerController.Instance.ActivePlayerIDs.ElementAt(ActivePlayerIndexTurn).Key;
            photonView.RPC("StartTurn", RpcTarget.AllBuffered, ActivePlayerIndexTurn, CurrentGameRoundTurn, CurrentColoursTurn);
        }
    }

    /// <summary>Calculates and then triggers an RPC to all clients "TriggerPlayerPossesionUpdateCallback" and sends the information required to understand the current
    /// ownership % of the board</summary>
    private void SendPlayerOwnedPercentages()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        List<CellProperties.ColourType> playerColours = new List<CellProperties.ColourType>();

        foreach (CellProperties.ColourType col in PlayerController.Instance.ActivePlayerIDs.Keys)
        {
            playerColours.Add(col);
        }

        int[] ownedHexagons = new int[playerColours.Count];

        foreach (PointyHexPoint point in HexFightGridBehaviour.Instance.HexFightGrid)
        {
            CellProperties.ColourType colType = HexFightGridBehaviour.Instance.HexFightGrid[point].CurrentColourType;
            for (int i = 0; i < playerColours.Count; i++)
            {
                if (playerColours[i] != colType) continue;

                ownedHexagons[i]++;
                break;
            }
        }

        int[] colourTypes = playerColours.Cast<int>().ToArray();
        photonView.RPC("TriggerPlayerPossesionUpdateCallback", RpcTarget.AllBuffered, colourTypes, ownedHexagons);
    }

    /// <summary>
    /// The master will call this and tell you
    /// Which players turn it is, index for the PlayerController.ActivePlayers
    /// The current game round it is
    /// The colour type of the new turn
    /// </summary>
    /// <param name="activePlayerIndex"></param>
    /// <param name="currentGameRoundTurn"></param>
    /// <param name="colType"></param>
    [PunRPC]
    private void StartTurn(int activePlayerIndex, int currentGameRoundTurn, CellProperties.ColourType colType)
    {
        ActivePlayerIndexTurn = activePlayerIndex;
        CurrentGameRoundTurn = currentGameRoundTurn;
        ((GameObject)Instantiate(Resources.Load("Prefabs/Popups/Your_Turn_Popup"))).GetComponent<TurnPopup>().Animate(colType);

        TriggerStartNewPlayerTurn();

        //Loop through all of the player objects, find the Player component. If its the active player set it to flash
        foreach (CellProperties.ColourType col in PlayerController.Instance.ActivePhotonIDs.Keys)
        {
            PlayerController.Instance.GetPlayerObject(col).GetComponent<HexPlayer>().Animating = colType == col;
        }
    }

    /// <summary>Called to all clients to show the end screen when the master client has determined the game has finished</summary>
    [PunRPC]
    private void ShowEndScreen()
    {
        ActivePlayerIndexTurn = 0;
        CurrentGameRoundTurn = MaxGameRounds;
        TriggerEndGameCallbacks();

        CurrentRoundState = GameState.END_GAME;

        HexFightGridBehaviour.Instance.SetClickingActive(false);
        foreach (PointyHexPoint point in HexFightGridBehaviour.Instance.HexFightGrid)
        {
            HexFightGridBehaviour.Instance.HexFightGrid[point].Active = false;
        }

        GameObject gameInfo = GameObject.Find("Game_Round_Info");
        ExpandUIButton expandUI = gameInfo.GetComponent<ExpandUIButton>();
        expandUI.ForceOpen();
    }

    //************** ---------- ****************//

    public void Awake()
    {
        if (Instance == null)
        {
            PhotonNetwork.IsMessageQueueRunning = true;
            PhotonNetwork.SendRate = 20;
            PhotonNetwork.SerializationRate = 10;
            Instance = this;
        }
        else Destroy(this);
    }

    // Use this for initialization
    void Start()
    {
        RoundType = GameRoundType.TURN_BASED;
        CurrentRoundState = GameState.PRE_GAME;

        if (RoundType == GameRoundType.TURN_BASED) TriggerStartNewPlayerTurn();
    }

    /// <summary>A player has connected to the room, increase the player count
    /// If you are the master and you are in the pregame state, then you will need to begin the count down before the game starts
    /// If you are the master and 4 people have joined, lock the room and begin the game in 5 seconds.
    /// If you are the master and the player count is now 1, then stop the timer</summary>
    [PunRPC]
    public void PlayersConnectedUpdate(int newPlayerCount)
    {
        PlayersConnected = newPlayerCount;
        float time = 15f;
        float countDownTime = PlayersConnected == 4 ? time : PlayersConnected == 3 ? time : time;//2 and 1
        PregameCountdown.Instance.photonView.RPC("CountDown", RpcTarget.MasterClient, PlayersConnected >= 1, countDownTime);
    }

    /// <summary>Once the initial count down has begun, the game will begin</summary>
    [PunRPC]
    public void StartGame()
    {
        CurrentGameRoundTurn = 0;

        photonView.RPC("SetRoundState", RpcTarget.AllBuffered, GameState.IN_GAME);
        HexFightGridBehaviour.Instance.GetComponent<PhotonView>().RPC("SetClickingActive", RpcTarget.AllBuffered, true);

        ActivePlayerIndexTurn = 0;
        CurrentColoursTurn = PlayerController.Instance.ActivePlayerIDs.ElementAt(ActivePlayerIndexTurn).Key;

        if (PhotonNetwork.IsMasterClient)
        {
            //Lock the room from other people joining
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;

            photonView.RPC("StartTurn", RpcTarget.AllBuffered, ActivePlayerIndexTurn, CurrentGameRoundTurn, CurrentColoursTurn);

            SendPlayerOwnedPercentages();
        }

        TriggerStartNewPlayerTurn();
    }
}
